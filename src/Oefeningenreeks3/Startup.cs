﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Oefeningenreeks3
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes
                .MapRoute(
                    name: "JSLijst",
                    template: "MaakLijst/JS",
                    defaults: new { controller = "Band", action = "JSLijst" }
                )
                .MapRoute(
                    name: "MaakBandLidFix",
                    template: "Maak{controller}/Naam/{bandlidnaam}/Jaar/{bandlidjaar}/Geslacht/{geslacht}/Band/{bandnaam}",
                    defaults: new { controller = "BandLid", action = "Maak" }
                )
                .MapRoute(
                    name: "MaakBandFix",
                    template: "Maak{controller}/Naam/{bandnaam}/Jaar/{bandjaar}",
                    defaults: new { controller = "Band", action = "Maak" }
                )
                .MapRoute(
                    name: "MaakBandLid",
                    template: "Maak/{controller}/{bandlidnaam}/{bandlidjaar}/{geslacht}/{bandnaam}",
                    defaults: new { controller = "BandLid", action = "Maak" }
                )
                .MapRoute(
                    name: "MaakBand",
                    template: "Maak/{controller}/{bandnaam}/{bandjaar}",
                    defaults: new { controller = "Band", action = "Maak" }
                )
                .MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
